#include "inc/malloc.h"
#include "mini_libft/libft.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void test_correct()
{
    char *test_loop[127];
    for (size_t i = 0; i < 127; i++)
    {
        test_loop[i] = malloc(10 * sizeof(char));
    }
    show_alloc_mem();
    for (size_t i = 0; i < 127; i++)
    {
        for (size_t j = 0; j < 10 * sizeof(char); j++)
        {
            test_loop[i][j] = '0';
        }
        free(test_loop[i]);
    }
    //show_alloc_mem();
}

void test_long_2()
{
    ft_mini_printf("START test long 2\n");
    char *ptr1[150];
    char *ptr2[150];
    char *ptr3[150];

    for (size_t i = 0; i < 150; i++)
    {
        int len1 = (TINY_SIZE - 2) / sizeof(char);
        ptr1[i] = malloc(TINY_SIZE - 2);
        for (size_t j = 0; j < len1; j++)
        {
            ptr1[i][j] = '1';
        }
    }
    for (size_t i = 0; i < 150; i++)
    {
        int len2 = (SMALL_SIZE - 2) / sizeof(char);
        ptr2[i] = malloc(SMALL_SIZE - 2);
        for (size_t j = 0; j < len2; j++)
        {
        //    ptr2[i][j] = '2';
        }
    }
    for (size_t i = 0; i < 150; i++)
    {
        int len3 = (SMALL_SIZE + 50) / sizeof(char);
        ptr3[i] = malloc(SMALL_SIZE + 50);
        for (size_t j = 0; j < len3; j++)
        {
            ptr3[i][j] = '3';
        }
    }
    show_alloc_mem();
    printf("FREE long test 2\n");
    for (size_t i = 0; i < 150; i++)
    {
        /*for (size_t j = 0; j < (SMALL_SIZE - 2); j++)
        {
            ptr2[i][j];
        }*/
        //printf("%p\n", ptr2[i]);
        free(ptr2[i]);
    }
    for (size_t i = 0; i < 150; i++)
    {
        /*
        for (size_t j = 0; j < (TINY_SIZE - 2); j++)
        {
            ptr1[i][j];
        }*/
        free(ptr1[i]);
    }
    for (size_t i = 0; i < 150; i++)
    {
        /*for (size_t j = 0; j < SMALL_SIZE + 50; j++)
        {
            ptr3[i][j];
        }*/
        free(ptr3[i]);
    }
    show_alloc_mem();
    printf("END test long 2\n");
}

void test_long()
{
    printf("START test long\n");
    void *ptr1[150];
    void *ptr2[150];
    void *ptr3[150];

    for (size_t i = 0; i < 150; i++)
    {
        ptr1[i] = malloc(TINY_SIZE - 2);
        for (size_t j = 0; j < (TINY_SIZE - 2); j++)
        {
            ptr1[i][j];
        }
    }
    for (size_t i = 0; i < 150; i++)
    {
        ptr2[i] = malloc(SMALL_SIZE - 2);
        for (size_t j = 0; j < (SMALL_SIZE - 2); j++)
        {
            ptr2[i][j];
        }
    }
    for (size_t i = 0; i < 150; i++)
    {
        ptr3[i] = malloc(SMALL_SIZE + 50);
        for (size_t j = 0; j < SMALL_SIZE + 50; j++)
        {
            ptr3[i][j];
        }
    }
    show_alloc_mem();
    printf("FREE long test\n");
    for (size_t i = 0; i < 150; i++)
    {
        for (size_t j = 0; j < (SMALL_SIZE - 2); j++)
        {
            ptr2[i][j];
        }
        free(ptr2[i]);
    }
    for (size_t i = 0; i < 150; i++)
    {
        for (size_t j = 0; j < (TINY_SIZE - 2); j++)
        {
            ptr1[i][j];
        }
        free(ptr1[i]);
    }
    for (size_t i = 0; i < 150; i++)
    {
        for (size_t j = 0; j < SMALL_SIZE + 50; j++)
        {
            ptr3[i][j];
        }
        free(ptr3[i]);
    }
    show_alloc_mem();
    printf("END test long\n");
}

void     ft_testmem(int *mem, size_t len)
{
    for (size_t i = 0; i < len; i++)
    {
        mem[i] = 12;
    //    printf("%c", mem[i]);
    }
    //printf("\n");
}

void    test_special()
{
    char *ptr1 = malloc(SMALL_SIZE - 2);
    for (size_t i = 0; i < SMALL_SIZE - 2; i++)
    {
        ptr1[i] = '1';
    }
    free(ptr1);
    ptr1 = malloc(SMALL_SIZE + 20);
    for (size_t i = 0; i < SMALL_SIZE + 20; i++)
    {
        ptr1[i] = '2';
    }
    show_alloc_mem();
}

void    test_ls()
{
    void *ptr[40];
    ptr[0] = malloc(736);
    ptr[1] = malloc(32);
    ptr[2] = malloc(32);
    ptr[3] = malloc(32);
    ptr[4] = malloc(32);
    ptr[6] = malloc(256);
    ptr[7] = malloc(1);
    free(ptr[6]);
    free(ptr[7]);
    ptr[8] = malloc(256);
    free(ptr[8]);
    ptr[9] = malloc(256);
    ptr[10] = malloc(1);
    free(ptr[9]);
    free(ptr[10]);
    ptr[11] = malloc(15);
    ptr[12] = malloc(45);
    ptr[13] = malloc(112);
    ptr[14] = malloc(45);
    ptr[15] = malloc(50);
    ptr[16] = malloc(47);
    ptr[17] = malloc(15);
    ptr[18] = malloc(64);
    ptr[19] = malloc(49);
    ptr[20] = malloc(67);
    ptr[21] = malloc(52);
    ptr[22] = malloc(54);
    ptr[23] = malloc(12);
    ptr[24] = malloc(45);
    ptr[25] = malloc(56);
    free(NULL);
    ptr[27] = malloc(16);
    ptr[28] = malloc(64);
    ptr[29] = malloc(14);
    ptr[30] = malloc(256);
    free(ptr[30]);
    ptr[31] = malloc(4096);
    ptr[32] = malloc(2160);
    free(ptr[31]);
    ptr[33] = malloc(3312);
    ptr[34] = malloc(4096);
    ptr[35] = malloc(1);
    free(ptr[33]);
    free(ptr[34]);
}

void    test_random(int length)
{
    srand(time(NULL));
    for (size_t i = 0; i < length; i++)
    {
        void *ptr = malloc(rand() % (SMALL_SIZE + 10));
        if (((int)ptr % 16) == 0) {
            ft_mini_printf("Alligned to 16 %d\n", (int)ptr % 16 );
        }
        else {
            ft_mini_printf("Not alligned to 16 %d\n", (int)ptr % 16 );
        }
    }
    show_alloc_mem();
}

void    test_show_alloc_mem()
{
    malloc(42);
    malloc(84);
    malloc(3725);
    malloc(48847);
    show_alloc_mem();
}

void    test_real_ls()
{
    system("DYLD_FORCE_FLAT_NAMESPACE=1 \
        DYLD_LIBRARY_PATH=. \
        DYLD_INSERT_LIBRARIES=libft_malloc.so \
        ls");
}

void    test_calloc(int nb)
{
    int *ptr;
    ptr = calloc(nb, sizeof(int));
    for (int i = 0; i < nb; i++)
    {
        ft_mini_printf("%d\n", ptr[i]);
    }
}

int main(int argc, char const *argv[])
{
    //show_alloc_mem();
    ft_mini_printf("TEST %d %d %d %d sizeof(t_heap)%d sizeof(t_block)%d\n", TINY_SIZE, SMALL_SIZE, TINY_HEAP, SMALL_HEAP, sizeof(t_heap), sizeof(t_block));
    //printf("sizeof: %d %d %d %d\n", sizeof(char), sizeof(void *), sizeof(int), sizeof(long));
    //test_long();
    //test_show_alloc_mem();
    //test_correct();
    //test_long_2();
    //test_special();
    //test_ls();
    //test_real_ls();
    //test_random(100);
    test_calloc(10);
    return 0;
}