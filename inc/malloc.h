#ifndef MALLOC_H
#define MALLOC_H
#include <sys/mman.h>
#include "../mini_libft/libft.h"

#include <stdlib.h>
#include <unistd.h>

#define BLOCK_SIZE 20

# define MALLOC_n
# define MALLOC_N
# define MALLOC_m
# define MALLOC_M


/* # define TINY_HEAP 20480 // 5 * getpagesize()
# define SMALL_HEAP 491520 // 120 * getpagesize()

# define TINY_SIZE 160
# define SMALL_SIZE 3840*/

# define TINY_HEAP 5 * getpagesize()
# define SMALL_HEAP 120 * getpagesize()

# define TINY_SIZE (TINY_HEAP / 128)
# define SMALL_SIZE (SMALL_HEAP / 128)

#define TINY_TYPE 0
#define SMALL_TYPE 1
#define LARGE_TYPE 2

#define FREE 0
#define NOFREE 1

typedef struct s_block t_block;
struct	s_block {
	struct s_block	*prev;
	struct s_block	*next;
	size_t			size;
	int				free:1;
}		__attribute__((aligned(16)));

typedef struct s_heap t_heap;
struct	s_heap {
	struct s_heap	*prev;
	struct s_heap	*next;
	int				type;
	size_t			total_size;
	size_t			free_size;
};//		__attribute__((aligned(16)));

//# define SIZEOF_HEAP sizeof(t_heap)

/* struct	s_block {
	size_t size;
	int free;
	t_block prev;
	t_block next;
	void	*ptr;
	char		data[1];
};
*/
extern t_heap *BaseList;

void	free(void *ptr);
void	*malloc(size_t size);
void	*realloc(void *ptr, size_t size);
//void	*calloc(size_t num, size_t size);

void show_alloc_mem();


size_t  *call_nmap(size_t len);
t_block	*heap_shift(t_heap *h);
void	*block_shift(t_block *b);
int		get_size(size_t size);
int     ft_align_memory(int size);


#endif
