ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

CC = gcc
FLAGS =
RM = rm -f
LIB_NAME = libft_malloc_$(HOSTTYPE).so
LIB_LINK_NAME = libft_malloc.so
LIBFT = mini_libft

SRC_PATH = ./src/
OBJ_PATH = ./obj/

SRC = malloc.c show_alloc_mem.c free.c realloc.c utils.c
INC = inc/malloc.h
SRC_TEST = main_tester.c
TEST_NAME = test
OBJ_NAME = $(SRC:.c=.o)
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

all: $(LIB_NAME)

$(LIB_NAME): $(OBJ) Makefile $(INC)
	@make -C $(LIBFT)
	gcc -shared -o $(LIB_NAME) $(OBJ) $(LIBFT)/libft.a
	ln -sf $(LIB_NAME) $(LIB_LINK_NAME)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	$(CC) $(FLAGS) -g -o $@ -c $<

test: $(LIB_NAME) $(SRC_TEST)
	@make -C $(LIBFT)
	$(CC) $(FLAGS) -g -o $(TEST_NAME) $(SRC_TEST) $(LIBFT)/libft.a $(LIB_NAME)

.PHONY: clean fclean re

clean:
	@make clean -C $(LIBFT)
	@$(RM) $(OBJ)

fclean: clean
	@make fclean -C $(LIBFT)
	@$(RM) $(LIB_NAME) $(LIB_LINK_NAME) $(TEST_NAME)

re: fclean all