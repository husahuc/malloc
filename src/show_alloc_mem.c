/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   show_alloc_mem.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: husahuc <husahuc@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/14 10:13:00 by husahuc       #+#    #+#                 */
/*   Updated: 2021/06/14 12:38:51 by husahuc       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/malloc.h"
#include <stdio.h>

t_heap      *BaseList;

void	print_hexa(unsigned int hex)
{
	if (hex < 16)
	{
		if (hex < 10)
			ft_putchar(hex + '0');
		else
			ft_putchar((hex - 10) + 'A');
	}
	else {
		print_hexa(hex/ 16);
		unsigned int hex_test = hex % 16;
		if (hex_test < 10)
			ft_putchar(hex_test + '0');
		else
			ft_putchar((hex_test - 10) + 'A');
	}
}

void	print_pointer(void *p)
{
	ft_putstr("0x");
	// 4086 to remove the 3 empty last digits, 16^3
	print_hexa((unsigned int) p / 4096);
}

void	print_type_heap(t_heap *h)
{
	if (h->type == TINY_TYPE)
		ft_putstr("TINY");
	else if (h->type == SMALL_TYPE)
		ft_putstr("SMALL");
	else
		ft_putstr("LARGE");
}

void show_alloc_mem() {
	t_heap *h = BaseList;
	t_block *b;
	size_t	total = 0;

	while (h) {
		print_type_heap(h);
		//ft_putstr("\n");
		ft_mini_printf("%p\n", h);
		//printf("\n", h);
		if (h->type != LARGE_TYPE)
		{
			b = ((void*)h + sizeof(t_heap));
			while (b)
			{
				if (b->free == NOFREE)
				{
					ft_mini_printf("%p - %p : %u bytes\n", block_shift(b), b->next, b->size);
					total += b->size;
				}
				b = b->next;
			}
		}
		else
		{
			//ft_mini_printf("LARGE %u, %p - %p\n", h->total_size, h + sizeof(t_heap), h + sizeof(t_heap) + h->total_size);
			ft_mini_printf("%p %p : %u bytes\n", heap_shift(h), h->next, h->total_size);
			total += h->total_size;
		}
		h = h->next;
	}
	ft_mini_printf("Total : %u bytes\n", total);
}