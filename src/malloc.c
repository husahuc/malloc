#include "../inc/malloc.h"
#include <stdio.h>

t_heap      *BaseList = NULL;

t_heap	*new_heap(int type, size_t size, t_heap *prev)
{
	t_heap *h;
	h = call_nmap(size + sizeof(t_heap));
	if (!h)
		return (NULL);
	h->next = NULL;
	h->prev = prev;
	h->type = type;
	h->total_size = size;
	h->free_size = size - sizeof(t_heap);
    return h;
}

void	new_block(size_t size, t_block *b, t_block *prev)
{
	b->next = NULL;
	b->prev = prev;
	b->free = NOFREE;
	b->size = size;
	if (prev)
		prev->next = b;
}

t_heap	*new_heap_tiny_small(int type, size_t size, t_heap *prev)
{
   // ft_mini_printf("START new heap\n");
	if (type == TINY_TYPE)
    {
    	return (new_heap(TINY_TYPE, TINY_HEAP, prev));
    }
    else if (type == SMALL_TYPE)
    {
        return(new_heap(SMALL_TYPE, SMALL_HEAP, prev));
    }
    return (NULL);
}

void	*find_block(t_heap *h, size_t size)
{
//    printf("FIND block h");
	t_block *b = heap_shift(h);
	if (b == NULL)
	{
		new_block(size, b, NULL);
		h->free_size -= size + sizeof(t_block);
		return block_shift(b);
	}
	while (b->next)
	{
		if (b->size > size && b->free == FREE)
		{
			b->free = NOFREE;
			return block_shift(b);
		}
        b = b->next;
	}
   // ft_mini_printf("FIND BLOCK %d\n", h->free_size);
	new_block(size, block_shift(b) + size, b);
	h->free_size -= size + sizeof(t_block);
	return block_shift(b->next);
}

void	*search_heap(int type, size_t size)
{
	t_heap *h = BaseList;
	while (h->next)
	{
		if (h->type == type && h->free_size > size + sizeof(t_block))
		{
			return find_block(h, size);
		}
		h = h->next;
	}
    //ft_mini_printf("SEARCH HEAP add %p %p\n", h);
	h->next = new_heap_tiny_small(type, size, h);
	if (h->next)
		return find_block(h->next, size);
	return (NULL);
}

// go to the end of the list and add a new large Heap and return a pointer.
void	*add_large_heap(size_t size)
{
    //ft_mini_printf("LARGE start\n");
	t_heap	*h = BaseList;
	while (h->next) {
        h = h->next;
    }
	h->next = new_heap(LARGE_TYPE, size, h);
	if (h->next)
	{
		h = h->next;
        ft_mini_printf("LARGE end %d", h->type);
		return (heap_shift(h));
	}
	return (NULL);
}

int		pre_allocate(void)
{
	if (!BaseList)
	{
        ft_mini_printf("PRE\n");
		BaseList = new_heap(TINY_TYPE, TINY_HEAP, NULL);
        if (BaseList == NULL)
            return (0);
        t_heap *h = BaseList;
        h->next = new_heap(SMALL_TYPE, SMALL_HEAP, h);
		if (h->next == NULL)
			return (0);
        ft_mini_printf("PRE %d\n", BaseList->next->type);
	}
	return (1);
}

void	*test_malloc_2(size_t size)
{
	int type = get_size(size);

	if (!pre_allocate())
		return (NULL);
	if (type == LARGE_TYPE)
		return (add_large_heap(size));
	return (search_heap(type, size));
}

void *malloc(size_t size) {

    ft_mini_printf("MALLOC|%d", (int)size);
    size = ft_align_memory(size);
    void *result = test_malloc_2(size);
    ft_mini_printf("|result|%p|%d\n", result, (int)result % 16);
    return (result);
}