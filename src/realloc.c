#include "../inc/malloc.h"

void *realloc(void *ptr, size_t size)
{
	void	*new_ptr;

	//ft_mini_printf("REALLOC %p %d\n", ptr, size);
	if (size == 0 || ptr)
	{
		if (!(new_ptr = (void*)malloc(1)))
			return (NULL);
		free(ptr);
		return (new_ptr);
	}
	if (!(new_ptr = (void*)malloc(size)))
		return (NULL);
	if (ptr)
	{
		ft_memcpy(new_ptr, ptr, size);
		free(ptr);
	}
	return (new_ptr);	
}