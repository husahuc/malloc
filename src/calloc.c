#include "../inc/malloc.h"

void* calloc(size_t num, size_t size)
{
    ft_mini_printf("CALLOC %u %u\n", num, size);
    size_t *ptr;
    if (size == 0)
        return (NULL);
    if ((ptr = malloc(num * size)) == NULL) {
        return (NULL);
    }
    for (size_t i = 0; i < num; i++)
    {
        ptr[i] = 0;
    }
    return (ptr);
}