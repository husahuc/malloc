#include "../inc/malloc.h"
#include <stdio.h>
t_heap *BaseList;

t_block *fusion(t_block *b)
{
	//ft_mini_printf("FUSION\n");
	if (b->next && b->next->free == FREE) {
		b->size += sizeof(t_block) + b->next->size;
		b->next = b->next->next;
		if (b->next)
			b->next->prev = b;
	}
	return (b);
}

t_block	*get_block(void *p, t_heap *h)
{
	t_block *b = heap_shift(h);
	//ft_mini_printf("GET BLOCK 1 %d %d %p\n", h->type, h->free_size, h);
	while (b)
	{
	//	ft_mini_printf("GET BLOCK 2 %d %p %p %p\n", b->size, p, b, b->next);
		if (block_shift(b) == p)
		{
			return (b);
		}
		b = b->next;
	}
	return (NULL);
}

t_heap	*get_heap(void *p) {
	t_heap *h = BaseList;
	while (h->next)
	{
	//	ft_mini_printf("GET HEAP %p %p %p\n", heap_shift(h), h->next, p);
		if (h < p && (h->next > p || h->next == NULL))
		{
	//		ft_mini_printf("GET HEAP TYPE %d\n", h->type);
			return (h);
		}
		h = h->next;
	}
	return (NULL);
}

int valid_addr(void *p) {
	if (BaseList) {
		t_block *b;
		t_heap *h = get_heap(p);
		if (!h)
		{
			//ft_mini_printf("FREE 0.21\n"); 
			return (0);
		}
		if (h->type == LARGE_TYPE)
		{
	//		ft_mini_printf("FREE VALID 3\n");
			return (1);
		}
		else {
	//		ft_mini_printf("FREE VALID 4\n");
			//b = get_block(p, h);
			if ((b = get_block(p, h)) != NULL)
			{
	//			ft_mini_printf("FREE VALID 5\n"); 
				return (1);
			}
		}
	}
	//ft_mini_printf("FREE VALID 6\n");
	return (0);
}

void	free_heap(t_heap *h)
{
	//ft_mini_printf("FREE HEAP\n");
	if (h->prev) {
		h->prev->next = h->next;
		if (h->next)
			h->next->prev = h->prev;
	}
	else
	{
		BaseList = h->next;
		h->next->prev = NULL;
	}
	munmap(h, h->total_size + sizeof(t_heap));
}

void	free(void *p) {
	if (p == NULL)
		return ;
	t_block *b;
	t_heap *h;
	ft_mini_printf("FREE START %p\n", p);
	if (valid_addr(p))
	{
	//	ft_mini_printf("FREE VALID\n");
		h = get_heap(p);
		if (h->type == LARGE_TYPE)
		{
			free_heap(h);
		}
		else
		{
			b = get_block(p, h);
			b->free = FREE;
			if (b->prev && b->prev->free == FREE)
			{
				b = fusion(b->prev);
			}
			if (b->next)
			{
				if (b->next->free == FREE)
					b = fusion(b);
			}
			else {
				free_heap(h);
			}
		}
	}
	//ft_mini_printf("FREE END\n");
}