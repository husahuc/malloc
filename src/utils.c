#include "../inc/malloc.h"

size_t  *call_nmap(size_t len) {
    //ft_mini_printf("%d\n", len);
    return mmap(0, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
}

t_block	*heap_shift(t_heap *h)
{
	return ((void *)h + sizeof(t_heap));
}

void	*block_shift(t_block *b)
{
	return ((void *)b + sizeof(t_block));
}

int		get_size(size_t size)
{
	if (size < TINY_SIZE)
        return (TINY_TYPE);
    else if (size < SMALL_SIZE)
        return (SMALL_TYPE);
    else
        return (LARGE_TYPE);
}

int     ft_align_memory(int size)
{
    return ((size + 15) & ~15);
}