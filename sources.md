## Sources Malloc
 [Implementing malloc](https://moss.cs.iit.edu/cs351/slides/slides-malloc.pdf)
 > Explain the structure well. And Fragmentation of free.

[Malloc Tutorial EPITA](https://wiki-prog.infoprepa.epita.fr/images/0/04/Malloc_tutorial.pdf)
> Dinausors of 42. Old with sbrk, but usefull for the explanation.

[(funny) Why malloc is important](https://www.youtube.com/watch?v=_zD33Hrbo4Y)
> Just for fun, why malloc implementation and optimisation is important. They spent 3 month reimplementing a memory management.

[Malloc on the mac](https://www.cocoawithlove.com/2010/05/look-at-how-malloc-works-on-mac.html)
> Mallos is different in the mac, and separate heap between 3 regions for optimisation. Especially for Objective-C, for optimisation for the TINY regions.

[42 student guide](https://medium.com/a-42-journey/how-to-create-your-own-malloc-library-b86fedd39b96)
> Separate malloc between Heap and Block with the correct structure. Memory alignment in 16 for Vim to work. Script for testing my malloc with real program using DYLD.

[Malloc function return and others things](https://pvs-studio.com/en/blog/posts/cpp/0558/)

[Why prealocate](https://stackoverflow.com/questions/5225914/optimization-preallocate-a-chunk-of-heap-memory-prior-to-use-by-multiple-object)

    husahuc | husahuc.com