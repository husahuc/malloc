#include "libft.h"

void		mini_printf_string(va_list list, int fd)
{
	char	*str;

	str = va_arg(list, char *);
	ft_putstr_fd(str, fd);
}

void		mini_printf_unsigned(va_list list, int fd)
{
	unsigned int	x;

	x = (unsigned int) va_arg(list, unsigned int);
	ft_putnbr_fd((int)x, fd);
}

void		mini_printf_char(va_list list, int fd)
{
	char	c;

	c = (unsigned char) va_arg(list, int);
	ft_putchar_fd(c, fd);
}

void		mini_printf_int(va_list list, int fd)
{
	int		x;

	x = va_arg(list, int);
	ft_putnbr_fd(x, fd);
}

void		mini_printf_tab(va_list list, int fd)
{
	char **tab;
	int i;

	tab = (char **)va_arg(list, char **);
	i = 0;
	while (tab[i] != NULL)
	{
		ft_putstr_fd(tab[i], fd);
		ft_putchar_fd(' ', fd);
		i++;
	}
}

void	ft_print_hexa(unsigned int hex, int fd )
{
	if (hex < 16)
	{
		if (hex < 10)
			ft_putchar_fd(hex + '0', fd);
		else
			ft_putchar_fd((hex - 10) + 'A', fd);
	}
	else {
		ft_print_hexa(hex/ 16, fd);
		unsigned int hex_test = hex % 16;
		if (hex_test < 10)
			ft_putchar_fd(hex_test + '0', fd);
		else
			ft_putchar_fd((hex_test - 10) + 'A', fd);
	}
}

void		mini_printf_pointer(va_list list, int fd)
{
	void *p;

	p = va_arg(list, void *);
	ft_putstr_fd("0x", fd);
	ft_print_hexa((unsigned int)p, fd);
}

#define LEN_FUN_PRINT 6
const t_print print_fun_tab[LEN_FUN_PRINT] = {
	{'s', &mini_printf_string},
	{'c', &mini_printf_char},
	{'d', &mini_printf_int},
	{'t', &mini_printf_tab},
	{'p', &mini_printf_pointer},
	{'u', &mini_printf_unsigned}
};

int			mini_printf_options(const char *str, va_list list, int fd)
{
	int		i;
	int		j;

	i = 0;
	while (str[i])
	{
		if (str[i] == '%')
		{
			i++;
			j = 0;
			while (j < LEN_FUN_PRINT)
			{
				if (print_fun_tab[j].c == str[i])
				{
					print_fun_tab[j].function(list ,fd);
					i++;
				}
				j++;
			}
			/* if (str[i] == '%')
			{
				ft_putchar_fd('%', fd);
				i++;
			}*/
		}
		else
		{
			ft_putchar_fd(str[i], fd);
			i++;
		}
	}
	return (i);
}

int			ft_mini_printf(const char *str, ...)
{
	va_list	params;
	int i;

	va_start(params, str);
	i = mini_printf_options(str, params, 1);
	va_end(params);
	return (i);
}