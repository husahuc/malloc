/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   libft.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: husahuc <husahuc@student.42.fr>              +#+                     */
/*                                                   +#+                      */
/*   Created: 2018/10/02 15:57:27 by husahuc       #+#    #+#                 */
/*   Updated: 2021/06/10 14:43:19 by husahuc       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */


#ifndef LIBFT_H
# define LIBFT_H

#include <stddef.h>
#include <unistd.h>
# include <stdarg.h>

# define INFINITE 1

int			ft_atoi(const char *str);
void		ft_putstr(char const *s);
size_t		ft_strlen(const char *str);
void		ft_putnbr(int n);
void		ft_putchar(char c);
void		ft_putnbr_fd(int n, int fd);
void		ft_putchar_fd(char c, int fd);
void		ft_putstr_fd(char const *s, int fd);
int			ft_mini_printf(const char *str, ...);
//void		ft_memdel(void **ap);
void		*ft_memcpy(void *str1, const void *str2, size_t n);

typedef struct	s_print {
	char		c;
	void		(*function)(va_list list, int fd);
}				t_print;

#endif